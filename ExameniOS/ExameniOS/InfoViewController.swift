//
//  InfoViewController.swift
//  ExameniOS
//
//  Created by Miguel Esteban Alvarez Naranjo on 6/6/18.
//  Copyright © 2018 Miguel Esteban Alvarez Naranjo. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var nombre:String = ""
    var urlsegue:String = ""
    var arreglolabel: [String] = []
    var arreglovalue: [Int] = []
    
    @IBOutlet weak var AvarageLabel: UILabel!
    @IBOutlet weak var HiNameLabel: UILabel!
    
    @IBOutlet weak var tableInfo: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HiNameLabel.text = "Hi, " + nombre
        
        let urlString = "https://api.myjson.com/bins/182sje"
        let url = URL(string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!){
            (data, response, error) in
            
            guard let data = data else{  //con guard me aseguro que exita data, si no existe termina la funcion
                print("Error NO data")
                return
            }
            
            guard let Info = try? JSONDecoder().decode(MiguelRequestInfo.self, from: data) else {
                print ("Error decoding info")
                return
            }
            
            
//            print(Info.data[0].label)
//            print(Info.data[0].value)
//            print(Info.data[1].label)
//            print(Info.data[1].value)
//            print(Info.data[2].label)
//            print(Info.data[2].value)
//            print(Info.data[3].label)
//            print(Info.data[3].value)
            
            
            
            //self.ResultadoLabel.text = "\(weatherInfo.weather[0].description)" //se ejecuta en otra thread y al ser asincrona se demora en aparecer en pantalla
            
            DispatchQueue.main.async {
                //self.AvarageLabel.text = "Avarage: " + "\((Info.data[0].value + Info.data[1].value + Info.data[2].value + Info.data[3].value) / 4)"
                
                var suma:Int = 0
                
                for index in 0...3 {
                    self.arreglolabel.append("\(Info.data[index].label)")
                    self.arreglovalue.append(Info.data[index].value)
                    suma = suma + Info.data[index].value
                }
                self.AvarageLabel.text = "Avarage: " + "\((suma / 4))"


                self.tableInfo.reloadData()
                
            }
            
            
            
        }
        task.resume()
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arreglolabel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "celda", for: indexPath) as! InfoTableViewCell
        cell.label.text = arreglolabel[indexPath.row]
        cell.value.text = "\(arreglovalue[indexPath.row])"
        return cell
        
        
    }
    
    /*
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }*/
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
