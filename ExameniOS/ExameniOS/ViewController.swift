//
//  ViewController.swift
//  ExameniOS
//
//  Created by Miguel Esteban Alvarez Naranjo on 5/6/18.
//  Copyright © 2018 Miguel Esteban Alvarez Naranjo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var TitleLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    
    @IBOutlet weak var name: UITextField!
    var direccion:String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let urlString = "https://api.myjson.com/bins/72936"
        let url = URL(string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!){
            (data, response, error) in
            
            guard let data = data else{  //con guard me aseguro que exita data, si no existe termina la funcion
                print("Error NO data")
                return
            }
            
            guard let requestInfo = try? JSONDecoder().decode(MiguelRequest.self, from: data) else {
                print ("Error decoding Weather")
                return
            }
            
            
            print(requestInfo.viewTitle)
            //self.TitleLabel.text = "\(reqestInfo.request[0].viewTitle)" //se ejecunextLinkta en otra thread y al ser asincrona se demora en aparecer en pantalla
            
            DispatchQueue.main.async {
                self.TitleLabel.text = "\(requestInfo.viewTitle)"
                self.dateLabel.text = "\(requestInfo.date)"
                self.direccion = "\(requestInfo.nextLink)"
            }
            
            
            
        }
        task.resume()
        
        
    }

    
    @IBAction func NextButton(_ sender: Any) {
        performSegue(withIdentifier: "InfoSegueview", sender: self)
        
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier=="InfoSegueview"{
            let destination = segue.destination as! InfoViewController
            destination.nombre = name.text!
            destination.urlsegue = direccion
        }
    }
    /*destino
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }*/


}

