//
//  RequestMiguel.swift
//  ExameniOS
//
//  Created by Miguel Esteban Alvarez Naranjo on 5/6/18.
//  Copyright © 2018 Miguel Esteban Alvarez Naranjo. All rights reserved.
//

import Foundation


struct MiguelRequest: Decodable {
    let viewTitle: String
    let date: String
    let nextLink: String
    
}

struct MiguelRequestInfo: Decodable {
    let data: [MiguelInfo]
}

struct MiguelInfo: Decodable {
    let label: String
    let value: Int
    
}


